package id.artivisi.training.microservices.frontend.service;

import id.artivisi.training.microservices.frontend.dto.WalletTransaction;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient(name = "wallet", fallback = WalletServiceFallback.class)
public interface WalletService {
    @GetMapping("/wallet/{walletId}/transaction")
    Iterable<WalletTransaction> ambilDataTransaksiWallet(@PathVariable String walletId);

    @GetMapping("/hostinfo")
    Map<String, Object> informasiHostBackend();
}
